import React, { PropTypes } from 'react';
import classnames from 'classnames';

const AUIProgressTrackerStep = props => {
        let className = props.className;
        const { selected } = props;


        className = classnames(
            props.className,
            'aui-progress-tracker-step',
            {'aui-progress-tracker-step-current' : props.selected}
        );

        return (
                    <li className={className}>
                        <span>
                            {props.children}
                        </span>
                    </li>
                );
};

AUIProgressTrackerStep.defaultProps = {
    selected: false
};

AUIProgressTrackerStep.displayName = 'AUIProgressTrackerStep';
AUIProgressTrackerStep.propTypes = {
    className: PropTypes.string,
    selected: PropTypes.bool, 
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string
    ]),
};

export default AUIProgressTrackerStep;


