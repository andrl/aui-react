import React, { Component, PropTypes } from 'react';

const AUITable = props => {
    return (
        <table className="aui">
            {props.children}
        </table>
    );
};

AUITable.displayName = 'AUITable';
AUITable.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element)
    ])
};

export default AUITable;
