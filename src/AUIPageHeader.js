import React, { PropTypes } from 'react';

const AUIPageHeader = props => {
    return (
        <header className="aui-page-header">
            <div className="aui-page-header-inner">
                {props.headerImage}
                <div className="aui-page-header-main">
                    <h1>{props.headerText}</h1>
                    {props.breadcrumb}
                </div>
                <div className="aui-page-header-actions">
                    <div className="notification"></div>
                    <div className="aui-buttons">
                        {props.buttons}
                    </div>
                </div>                
            </div>
        </header>
    );
};

AUIPageHeader.displayName = 'AUIPageHeader';
AUIPageHeader.propTypes = {
    headerText: PropTypes.string,
    headerImage: PropTypes.element,
    breadcrumb: React.PropTypes.element,
    buttons: React.PropTypes.array
};

export default AUIPageHeader;
