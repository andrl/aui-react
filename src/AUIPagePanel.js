import React, { Component, PropTypes } from 'react';

const AUIPagePanel = props => {
    let nav;
    if (props.nav) {
        nav = (
            <div className="aui-page-panel-nav">
                {props.nav}
            </div>
        );
    }

    let aside;
    if (props.aside) {
        aside = (
            <div className="aui-page-panel-sidebar content-sidebar">
                {props.aside}
            </div>
        );
    }

    return (
        <div className="aui-page-panel" style={props.style}>
            <div className="aui-page-panel-inner">
                {nav}
                <section className="aui-page-panel-content content-body">
                    {props.children}
                </section>
                {aside}
            </div>
        </div>
    );
};

AUIPagePanel.displayName = 'AUIPagePanel';
AUIPagePanel.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array
    ]),
    nav: PropTypes.element,
    aside: PropTypes.element,
    style: PropTypes.object
};

export default AUIPagePanel;
