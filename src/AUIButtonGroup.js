import React, { Component, PropTypes } from 'react';

export default class AUIButtonGroup extends Component {
    render() {
        return (<p className="aui-buttons">{this.props.children}</p>);
    }
}

AUIButtonGroup.displayName = 'AUIButtonGroup';

AUIButtonGroup.propTypes = {
    children: PropTypes.array
};
