import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import AUIBlanket from './AUIBlanket';
import { body } from './facades/document';
import { addEventListener, removeEventListener } from './facades/window';

class AUIDialog extends Component {
    constructor(props) {
        super(props);
        this.oldOverflowStyleBody = '';

        this._onKeyDownEsc = this._onKeyDownEsc.bind(this);
    }

    componentDidMount() {
        // freeze scrolling ability of body
        this.oldOverflowStyleBody = body().style.overflow;
        body().style.overflow = 'hidden';

        addEventListener('keydown', this._onKeyDownEsc);
    }

    componentWillUnmount() {
        // unfreeze scrolling ability of body
        body().style.overflow = this.oldOverflowStyleBody;

        removeEventListener('keydown', this._onKeyDownEsc);
    }

    _onKeyDownEsc(event) {
        if (event.key === 'Escape' || event.keyCode === 27) {
            this.props.onClose();
        }
    }

    render() {
        const {
            type,
            size,
            titleContent,
            headerSecondaryContent,
            headerActionContent,
            footerActionContent,
            footerHintContent,
            styles,
            styleClass,
            closeButtonText,
            ...otherProps
        } = this.props;

        const classes = classnames([
            'aui-layer',
            'aui-dialog2',
            `aui-dialog2-${size}`,
            {'aui-dialog2-warning': type === 'warning'},
            styleClass
        ]);

        const mainHeader = titleContent ? <h2 className='aui-dialog2-header-main'>{titleContent}</h2> : undefined;
        const secondaryHeader = headerSecondaryContent ?
            <div className='aui-dialog2-header-secondary'>{headerSecondaryContent}</div> : undefined;
        const headerActions = headerActionContent ?
            <div className='aui-dialog2-header-actions'>{headerActionContent}</div> : undefined;
        const closeButton = type !== 'modal' ? (
            <a className='aui-dialog2-header-close' onClick={this.props.onClose}>
                <span className='aui-icon aui-icon-small aui-iconfont-close-dialog'>{closeButtonText}</span>
            </a>) : undefined;

        const dialogHeader = (
            <header className={`aui-dialog2-header ${this.props.headerStyleClass}`}>
                {mainHeader}
                {secondaryHeader}
                {headerActions}
                {closeButton}
            </header>
        );

        const dialogContent = (
            <div className={`aui-dialog2-content ${this.props.contentStyleClass}`}>
                {this.props.children}
            </div>
        );

        const footerActions = footerActionContent ?
            <div className='aui-dialog2-footer-actions'>{footerActionContent}</div> : undefined;
        const footerHints = footerHintContent ?
            <div className='aui-dialog2-footer-hint'>{footerHintContent}</div> : undefined;

        const dialogFooter = (
            <footer className={`aui-dialog2-footer ${this.props.footerStyleClass}`}>
                {footerActions}
                {footerHints}
            </footer>
        );
        return (
            <div>
                <section role='dialog' className={classes} style={styles} {...otherProps}>
                    {dialogHeader}
                    {dialogContent}
                    {dialogFooter}
                </section>
                <AUIBlanket onClick={ (type === 'modal') ? null : this.props.onClose} />
            </div>
        );
    }
};

AUIDialog.displayName = 'AUIDialog';
AUIDialog.propTypes = {
    /**
     * Type of the dialog including:
     * - default
     * - warning: which has a red header
     * - modal: which has no close icon at the top right corner and cannot be closed by clicking on the blanket behind it.
     */
    type: PropTypes.string,
    /**
     * Size of the dialog
     */
    size: PropTypes.oneOf(['small', 'medium', 'large', 'xlarge']).isRequired,
    /**
     * The main header content for dialog
     */
    titleContent: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]),
    /**
     * Dialog action actions to render on the right of the header
     */
    headerActionContent: PropTypes.arrayOf(PropTypes.element),
    /**
     * The secondary header content for dialog
     */
    headerSecondaryContent: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]),
    /**
     * Text for close button and icon
     */
    closeButtonText: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]),
    /**
     * Main dialog content
     */
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element,
        PropTypes.array
    ]),
    /**
     * Dialog footer hint to render on the left of the footer
     */
    footerHintContent: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]),
    /**
     * Dialog footer actions to render on the right of the footer
     */
    footerActionContent: PropTypes.arrayOf(PropTypes.element),
    /**
     * Custom style object for the whole dialog
     */
    styles: PropTypes.object,
    /**
     * Custom style class for the whole dialog
     */
    styleClass: PropTypes.string,
    /**
     * Custom style class for the dialog header
     */
    headerStyleClass: PropTypes.string,
    /**
     * Custom style class for the dialog content
     */
    contentStyleClass: PropTypes.string,
    /**
     * Custom style class for the dialog footer
     */
    footerStyleClass: PropTypes.string,
    /**
     * Callback for dialog close event
     */
    onClose: PropTypes.func
};

export default AUIDialog;
