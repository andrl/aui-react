import React, { PropTypes } from 'react';

const AUINavGroup = props => {
    return (
        <nav className="aui-navgroup aui-navgroup-vertical">
            <div className="aui-navgroup-inner">
                {props.children}
            </div>
        </nav>
    );
};

AUINavGroup.displayName = 'AUINavGroup';
AUINavGroup.propTypes = {
    children: PropTypes.node
};

export default AUINavGroup;
