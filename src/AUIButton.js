import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import AUIIcon from './AUIIcon';

const AUIButton = props => {
    const { icon, type, disabled, children, className, doSubmit, ...otherProps} = props;

    let iconComponent = null;

    if (icon) {
        iconComponent = <span><AUIIcon icon={icon}/>&nbsp;</span>;
    }
    let typeClass;
    if (type) {
        typeClass = 'aui-button-' + type;
    }

    let classes = classnames('aui-button', typeClass);
    if (className) {
        classes += ` ${className}`;
    }
    
    let buttonType = doSubmit?"submit":"button";

    return (
        <button className={classes} aria-disabled={disabled} type={buttonType}{...otherProps}>{iconComponent}{children}</button>
    );
};

AUIButton.defaultProps = {
    className: '',
    doSubmit: false
};

AUIButton.displayName = 'AUIButton';
AUIButton.propTypes = {
    /**
     * The type of the button link, primary or subtle
     */
    type: PropTypes.oneOf(['link', 'primary', 'subtle']),
    /**
     * The icon to show in the button
     */
    icon: PropTypes.string,
    /**
     * Disabled
     */
    disabled: PropTypes.bool,
    /**
     * The children of the button
     */
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string
    ]),
    className: PropTypes.string,
    doSubmit: PropTypes.bool
};

export default AUIButton;
