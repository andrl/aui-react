import React, { PropTypes } from 'react';
import classnames from 'classnames';

const AUIProgressTracker = props => {
        let className = props.className;
        const { inverted, children} = props;


        className = classnames(
            props.className,
            'aui-progress-tracker',
            {'aui-progress-tracker-inverted' : props.inverted}
        );

        return (
            <ol className={className}> 
                <div className="field-group">
                    {children}
                </div>
            </ol>);
};

AUIProgressTracker.defaultProps = {
    inverted: false
};

AUIProgressTracker.displayName = 'AUIProgressTracker';
AUIProgressTracker.propTypes = {
    className : PropTypes.string,
    inverted : PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array,
        PropTypes.string
    ])
    
};

export default AUIProgressTracker;

