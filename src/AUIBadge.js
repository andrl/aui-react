import React, { Component, PropTypes } from 'react';

/**
 * Badges provide quick visual identification for numeric values such as tallies and other quantities. They are not used
 * for anything other than integers. For statuses, use Lozenges. To call out tags or other high-visibility attributes,
 * use Labels.
 */
export default class AUIBadge extends React.Component {
    render() {
        return (
            <span className="aui-badge" id={this.props.id}>{this.props.text}</span>
        );
    }
}

AUIBadge.displayName = 'AUIBadge';

AUIBadge.propTypes = {
    /**
     * Visible text of the badge (usually a number).
     */
    text: React.PropTypes.string.isRequired,
    /**
     * ID attribute.
     */
    id: React.PropTypes.string
};
