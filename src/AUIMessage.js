import React from 'react';
import classnames from 'classnames';

const AUIMessage = ({type, title, children, isCloseable, onClose}) => {
    let typeClass;
    if (type) {
        typeClass = 'aui-message-' + type;
    }
    const classes = classnames('aui-message', typeClass, {closeable: isCloseable});

    return (
        <div className={classes}>
            {!title ? null : <p className="title"><strong>{title}</strong></p>}
            { isCloseable ? <span className="aui-icon icon-close" role="button" tabIndex="0" onClick={onClose}></span> : null }
            {children}
        </div>
    );
};

AUIMessage.displayName = 'AUIMessage';
AUIMessage.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.element
    ]).isRequired,
    title: React.PropTypes.string,
    isCloseable: React.PropTypes.bool,
    type: React.PropTypes.oneOf(['warning', 'error', 'success', 'hint', 'info']),
    /**
     * Callback for closing AUIMessage
     */
    onClose: React.PropTypes.func,
};

export default AUIMessage;
