import React, { PropTypes } from 'react';
import AUIPagePanel from './AUIPagePanel';

const AUIPageContent = props => {
    return (
        <section id="content" role="main" className={props.className}>
            {props.pageHeader}
            {props.sidebar}
            <AUIPagePanel nav={props.nav} aside={props.aside} style={props.style}>{props.children}</AUIPagePanel>
        </section>
    );
};

AUIPageContent.displayName = 'AUIPageContent';
AUIPageContent.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.array
    ]),
    nav: PropTypes.element,
    aside: PropTypes.element,
    sidebar: PropTypes.element,
    pageHeader: PropTypes.element,
    style: PropTypes.object,
    className: PropTypes.string
};

export default AUIPageContent;
