import React from 'react';
import classnames from 'classnames';

const AUIApplicationLogo = ({logo, text, children, headerLink}) => {
    let logoClass;
    if (logo) {
        logoClass = 'aui-header-logo-' + logo;
    }
    const classes = classnames('aui-header-logo', {'aui-header-logo-textonly': text}, logoClass);
    return (
        <h1 className={classes} id="logo">
            <a href={headerLink}>
                <span className="aui-header-logo-device">{children || text}</span>
            </a>
        </h1>
    );
};

AUIApplicationLogo.defaultProps = {
    headerLink: '/'
};

AUIApplicationLogo.displayName = 'AUIApplicationLogo';
AUIApplicationLogo.propTypes = {
    children: React.PropTypes.element,
    headerLink: React.PropTypes.string,
    logo: React.PropTypes.oneOf(['aui', 'bamboo', 'bitbucket', 'confluence', 'crowd', 'fecru', 'hipchat', 'jira', 'stash']).isRequired,
    text: React.PropTypes.string
};

export default AUIApplicationLogo;
