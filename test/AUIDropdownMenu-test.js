import React from 'react';
import { render } from 'enzyme';

import { expect } from 'chai';
import AUIDropdownMenu from '../src/AUIDropdownMenu';

describe('AUIDropdownMenu', () => {
    it('should render the correct AUI web component', () => {
        expect(render(<AUIDropdownMenu />).html()).to.equal(`<div class="aui-dropdown2 aui-style-default"></div>`);
    });

    it('should render child inside the AUI web component', () => {
        expect(render(<AUIDropdownMenu>test</AUIDropdownMenu>).html()).to.equal(`<div class="aui-dropdown2 aui-style-default">test</div>`);
    });
});
