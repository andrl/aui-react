import React from 'react';
import { render, shallow } from 'enzyme';

import AUIApplicationHeader from '../src/AUIApplicationHeader';
import AUIApplicationLogo from '../src/AUIApplicationLogo';

import proxyquire from 'proxyquire';
import { createStubComponent } from './StubComponent';

proxyquire.noCallThru();

import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
chai.use(chaiEnzyme());

describe('AUIApplicationHeader', () => {
    describe('Markup', () => {
        let StubbedAUIApplicationHeader;
        beforeEach(() => {
            StubbedAUIApplicationHeader = proxyquire('../src/AUIApplicationHeader', {
                './AUIApplicationLogo': createStubComponent('AUIApplicationLogo'),
                './AUINav': createStubComponent('AUINav')
            }).default;
        });
        /*eslint-disable*/
        it('should render the correct markup for the header', () => {
            expect(render(<AUIApplicationHeader />).html()).to.equal(`<header id="header" role="banner"><nav class="aui-header aui-dropdown2-trigger-group" data-aui-response="true" role="navigation"><div class="aui-header-inner"><div class="aui-header-primary"><h1 class="aui-header-logo" id="logo"><a href="/"><span class="aui-header-logo-device"></span></a></h1></div><div class="aui-header-secondary"></div></div></nav></header>`);
        });
        /*eslint-enable*/
    });

    it('should render a logo with a headerLink of "/" by default', () => {
        const wrapper = shallow(<AUIApplicationHeader logo="confluence" />);

        expect(wrapper).to.have.descendants('AUIApplicationLogo');
        expect(wrapper).to.have.descendants('[headerLink="/"]');
    });

    it('should render a logo with a headerLink of "/confluence"', () => {
        const wrapper = shallow(<AUIApplicationHeader logo="confluence" headerLink="/confluence" />);

        expect(wrapper).to.have.descendants(AUIApplicationLogo);
        expect(wrapper).to.have.descendants('[headerLink="/confluence"]');
    });
});
