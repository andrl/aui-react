import React from 'react';
import { render } from 'enzyme';

import { expect } from 'chai';
import AUILabel from '../src/AUILabel';

describe('AUILabel', () => {
    it('should render the correct AUI markup', () => {
        expect(render(<AUILabel>My Label</AUILabel>).html()).to.equal(`<a class="aui-label">My Label</a>`);
    });
});
