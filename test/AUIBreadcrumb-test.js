import React from 'react';
import { render } from 'enzyme';
import { expect } from 'chai';
import AUIBreadcrumb from '../src/AUIBreadcrumb';

describe('AUIBreadcrumb', () => {
    it('should render the correct AUI markup', () => {
        expect(render(<AUIBreadcrumb />).html()).to.equal('<li><a href="#"><span class="aui-nav-item-label"></span></a></li>');
    });
});
