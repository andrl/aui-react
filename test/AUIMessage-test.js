import React from 'react';
import { render, shallow } from 'enzyme';

import { expect } from 'chai';
import AUIMessage from '../src/AUIMessage';

describe('AUIMessage', () => {
    it('should render to correct AUI markup when there is no title', () => {
        expect(render(<AUIMessage>Hello world</AUIMessage>).html()).to.equal(`<div class="aui-message">Hello world</div>`);
    });

    it('should render to correct AUI markup when there is a title', () => {
        expect(render(<AUIMessage title="NOTE">Hello world</AUIMessage>).html()).to.equal(`<div class="aui-message"><p class="title"><strong>NOTE</strong></p>Hello world</div>`);
    });

    it('should render the correct class for an error message', () => {
        expect(render(<AUIMessage type="error">Error</AUIMessage>).html()).to.equal(`<div class="aui-message aui-message-error">Error</div>`);
    });

    it('should render the correct class for a warning message', () => {
        expect(render(<AUIMessage type="warning">Warning</AUIMessage>).html()).to.equal(`<div class="aui-message aui-message-warning">Warning</div>`);
    });

    it('should render the correct class for a success message', () => {
        expect(render(<AUIMessage type="success">Success</AUIMessage>).html()).to.equal(`<div class="aui-message aui-message-success">Success</div>`);
    });

    it('should render the correct class for a hint', () => {
        expect(render(<AUIMessage type="hint">Hint</AUIMessage>).html()).to.equal(`<div class="aui-message aui-message-hint">Hint</div>`);
    });

    it('should render the correct class for an info message', () => {
        expect(render(<AUIMessage type="info">Info</AUIMessage>).html()).to.equal(`<div class="aui-message aui-message-info">Info</div>`);
    });

    it('should render close icon when isCloseable=true', () => {
        const wrapper = shallow(<AUIMessage type="info" isCloseable onClose={() => {}}>Info</AUIMessage>);
        const closeIcon = wrapper.find('.aui-icon.icon-close');
        expect(closeIcon).to.exist;
        expect(closeIcon).to.have.prop('onClick', wrapper.prop('onClose'));
    });

    it('should not render close icon when isCloseable=false', () => {
        const wrapper = shallow(<AUIMessage type="info">Info</AUIMessage>);
        const closeIcon = wrapper.find('.aui-icon.icon-close');
        expect(closeIcon).to.not.exist;
    });
});
