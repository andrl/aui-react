import React from 'react';
import { render } from 'enzyme';

import { expect } from 'chai';
import AUISection from '../src/AUISection';

describe('AUISection', () => {
    it('should render to correct AUI webcomponent', () => {
        expect(render(<AUISection>Hello world</AUISection>).html()).to.equal(`<aui-section>Hello world</aui-section>`);
    });

    it('should spread properties to the AUI webcomponent', () => {
        const mockStyles = { marginTop:20 };
        expect(render(<AUISection style={mockStyles}>Hello world</AUISection>).html()).to.equal(`<aui-section style="margin-top:20px;">Hello world</aui-section>`);
    });
});
