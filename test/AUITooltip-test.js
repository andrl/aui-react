import React from 'react';
import { render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import AUITooltip from '../src/AUITooltip';

chai.use(chaiEnzyme());

describe('AUITooltip', () => {
    it('should render the correct markup ', () => {
        const wrapper = render(<AUITooltip gravity='n'>Test</AUITooltip>);
        expect(wrapper.html()).to.equal(`<div role="tooltip" class="tipsy tipsy-n"><div class="tipsy-arrow tipsy-arrow-n"></div><div class="tipsy-inner">Test</div></div>`);
    });
});
